<?php

/**
 * @file
 * Contains \Drupal\google_api_client\Controller\GoogleAuthController.
 */

namespace Drupal\google_api_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\google_api_client\GoogleApiClient;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GoogleAuthController extends ControllerBase {

  private $client;

  public function __construct() {
    if (!empty($_GET['scope'])) {
      $this->client = GoogleApiClient::initClient($_GET['scope']);
    }
    else {
      $this->client = GoogleApiClient::initClient();
    }
  }

  public function login() {
    if(!$this->client) {
      $destination = !empty($_SESSION['google_api_client']['destination']) ? $_SESSION['google_api_client']['destination'] : '<front>';
      return new RedirectResponse($destination);
    }

    return new TrustedRedirectResponse($this->client->createAuthUrl(), 301);
  }

  public function auth() {
    $code = filter_input(INPUT_GET, 'code');
    $destination = !empty($_SESSION['google_api_client']['destination']) ? $_SESSION['google_api_client']['destination'] : '<front>';

    if(empty($code)) {
      \Drupal::logger('Google API Client')->notice('Exception: no code');
      return new RedirectResponse($destination);
    }

    if(!$this->client) {
      \Drupal::logger('Google API Client')->notice('Exception: no client');
      return new RedirectResponse($destination);
    }

    try {
      $result = $this->client->authenticate($code);
      \Drupal::logger('Google API Client')->notice('Auth result: %result', array('%result' => print_r($result, TRUE)));

      $token = $this->client->getAccessToken();
      \Drupal::logger('Google API Client')->notice('Auth Token: %token', array('%token' => print_r($token, TRUE)));

      $account = \Drupal::currentUser();
      if (!$account->isAnonymous() && $account->id()) {
        \Drupal::service('user.data')->set('google_api_client', $account->id(), 'access_token', $token);
      }
    }
    catch(\Exception $e) {
      \Drupal::logger('Google API Client')->notice('Exception: %message', array('%message' => $e->getMessage()));
    }

    return new RedirectResponse($destination);
  }
}

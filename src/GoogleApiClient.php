<?php

/**
 * @file
 * Contains \Drupal\google_api_client\GoogleApiClient.
 */

namespace Drupal\google_api_client;

use Drupal\Core\Url;
use Google_Client;
use Google_Auth_AssertionCredentials;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GoogleApiClient {

  public static function initClient(array $scopes = array()) {
    $config = \Drupal::config('google_api_client.config');

    try {
      $client = new Google_Client();
      $client->setClientId($config->get('client_id'));
      $client->setClientSecret($config->get('client_secret'));
      if (!empty($scopes)) {
        $client->setScopes($scopes);
      }
      $redirect_url = Url::fromRoute('google_api_client.auth', array(), array('absolute' => TRUE))->toString();
      $client->setRedirectUri($redirect_url);
      $client->setApplicationName('GoWeb');
      $client->setAccessType('offline');

      $account = \Drupal::currentUser();
      if (!$account->isAnonymous() && $account->id()) {
        $access_token = \Drupal::service('user.data')->get('google_api_client', $account->id(), 'access_token');
        if (!empty($access_token)) {
          $client->setAccessToken($access_token);
        }
      }

      if($client->getAccessToken() && $client->isAccessTokenExpired()) {
        $token = json_decode($client->getAccessToken());
        $client->refreshToken($token->access_token);
        \Drupal::service('user.data')->set('google_api_client', $account->id(), 'access_token', $client->getAccessToken());
      }

      return $client;
    }
    catch(\Exception $e) {
      \Drupal::logger('Google API Client')->notice('Exception: %message', array('%message' => $e->getMessage()));
    }

  }

  public static function getClient(array $scopes) {
    $client = self::initClient($scopes);

    if (!$client->getAccessToken()) {
      $_SESSION['google_api_client']['destination'] = \Drupal::request()->getRequestUri();
      // $authUrl = $client->createAuthUrl();
      $redirect_url = Url::fromRoute('google_api_client.login', [], ['query' => ['scope' => $scopes], 'absolute' => TRUE])->toString();
      //$redirect = new TrustedRedirectResponse($redirect_url);
      $redirect = new RedirectResponse($redirect_url);
      $redirect->send();
    }
    else {
      return $client;
    }
  }

  public static function setScopes(Google_Client &$client, array $scopes) {
    $client->setScopes($scopes);
  }

  public static function getClientByService(array $scopes) {
    $config = \Drupal::config('google_api_client.config');

    try {
      $credentials = new Google_Auth_AssertionCredentials(
        $config->get('service_account_id'),
        $scopes,
        file_get_contents(DRUPAL_ROOT . $config->get('service_account_key'))
      );

      $client = new Google_Client();
      $client->setAssertionCredentials($credentials);
      if ($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion();
      }

      return $client;
    }
    catch(\Exception $e) {
      \Drupal::logger('Google API Client')->notice('Exception: %message', array('%message' => $e->getMessage()));
    }

  }

}
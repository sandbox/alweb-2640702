<?php

/**
 * @file
 * Contains \Drupal\google_api_client\Form\GoogleApiClientConfigForm.
 */

namespace Drupal\google_api_client\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure google_api_client module.
 */
class GoogleApiClientConfigForm extends ConfigFormBase {

  /**
   * Constructs a GoogleApiClientConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_api_client_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['google_api_client.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_api_client.config');

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#maxlength' => 255,
      '#description' => t('Google API Client ID.'),
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#maxlength' => 255,
      '#description' => t('Google API Client Secret.'),
    );

    $form['service_account_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Service Account ID'),
      '#default_value' => $config->get('service_account_id'),
      '#maxlength' => 255,
      '#description' => t('Google API Service Account ID.'),
    );

    $form['service_account_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Service Account Key'),
      '#default_value' => $config->get('service_account_key'),
      '#maxlength' => 255,
      '#description' => t('Path to Google API Service Account Key.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('google_api_client.config')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('service_account_id', $form_state->getValue('service_account_id'))
      ->set('service_account_key', $form_state->getValue('service_account_key'))
    ;

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
